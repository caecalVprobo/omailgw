import { GetSettings, SetSettings, GetSMTPList, AddSMTP, DeleteSMTP } from "./api_connector.js";
import settings from './settings.js';
import showRightInfos from './commun.js';

showRightInfos();

// Update datas to show
jQuery(function() {
  UpdateSettings();
  UpdateSMTPList();
});

// Select html objects
const loadingSpinner = document.getElementById("loading-spinner");
const errorMessage = document.getElementById("errorMessage");
const formRapport = document.querySelector('#usersettingsForm');
const formNew = document.querySelector('#smtpauthForm');
const smtpLoadingSpinner = document.getElementById("smtp-loading-spinner");
const smtpErrorMessage = document.getElementById("smtp-errorMessage");
const smtpTable = document.getElementById("smtp-list");

async function UpdateSettings() {
  // Show loading spinner
  loadingSpinner.style.display = "block";
  // Clear previous  messages
  errorMessage.innerHTML = "";
  // Ask infos from API : Reports Settings
  let getSettingsTry = await GetSettings();
  if (getSettingsTry[0]) {
    formRapport.querySelector('select[name="rapport"]').selectedIndex = getSettingsTry[1];
    formRapport.querySelector('select[name="rapport_verbose"]').selectedIndex = getSettingsTry[2];
  } else {
    const errorReading = settings.errorcodes[getSettingsTry[1]];
    errorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + getSettingsTry[2] + " )") : "Couldn't access server.";
  }
  // Hide loading spinner
  loadingSpinner.style.display = "none";
}

async function UpdateSMTPList() {
  // Show loading spinner
  smtpLoadingSpinner.style.display = "block";
  // Clear previous messages
  smtpErrorMessage.innerHTML = "";
  // Ask infos from API : Reports Settings
  let getSmtpauthTry = await GetSMTPList();
  if (getSmtpauthTry[0]) {
    let smtpList = getSmtpauthTry[1];
    // First foreach adds rows
    smtpTable.innerHTML = "";
    for (let smtpAuth of smtpList){
      smtpTable.innerHTML +=
        `
        <tr>
          <td>${smtpAuth.username}</td>
          <td class='password'>
            <span class='passwordHide'>*******</span>
            <span class='passwordShow' style='display:none'>${smtpAuth.password}</span>
          </td>
          <td>${smtpAuth.description}</td>
          <td><button type='button' class='btn btn-danger delete-button' data-i18n-key="delete">Delete</button></td>
        </tr>
        `;

      // Add event listener to handle delete function
      smtpTable.querySelector('.delete-button:last-child').addEventListener('click', () => {
        Delete(smtpAuth.username);
      });
    };
    for (const row of smtpTable.querySelectorAll('tr')) {
      // Add event listener to toggle password display
      const passwordCell = row.querySelector('.password');
      passwordCell.addEventListener('click', () => {
        const passwordHide = passwordCell.querySelector('.passwordHide');
        const passwordShow = passwordCell.querySelector('.passwordShow');
        if (passwordHide.style.display === 'none') {
          passwordHide.style.display = '';
          passwordShow.style.display = 'none';
        } else {
          passwordHide.style.display = 'none';
          passwordShow.style.display = '';
        }
      });
      // Add event listener to the delete button
      row.querySelector('.delete-button').addEventListener('click', () => {
        const usernameCell = row.querySelector('td:first-child');
        const username = usernameCell.textContent.trim();
        Delete(username);
        row.remove(); // Remove the row from the table
      });
    }
  } else {
    const errorReading = settings.errorcodes[getSmtpauthTry[1]];
    smtpErrorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + getSmtpauthTry[2] + " )") : "Couldn't access server.";
  }
  // Hide loading spinner
  smtpLoadingSpinner.style.display = "none";
}

// Handle deletion action
async function Delete(email) {
  // Show loading spinner
  smtpLoadingSpinner.style.display = "block";
  // Clear previous messages
  smtpErrorMessage.innerHTML = "";

  // Handle the API response
  let deleteTry = await DeleteSMTP(email);
  if (deleteTry[0]) {
    UpdateSMTPList();
  } else {
    alert("Something went wrong.");
    const errorReading = settings.errorcodes[deleteTry[1]];
    smtpErrorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + deleteTry[2] + " )") : "Couldn't access server.";
  }
  // Hide loading spinner
  smtpLoadingSpinner.style.display = "none";
};

// Handle Settings Form
formRapport.addEventListener('submit', async (event) => {
  event.preventDefault();

  // Show loading spinner
  loadingSpinner.style.display = "block";

  const rapportFrequency = formRapport.querySelector('select[name="rapport"]').value;
  const rapportVerbose = formRapport.querySelector('select[name="rapport_verbose"]').value;

  // Clear previous & show messages
  errorMessage.innerHTML = "";

  // Handle the API response
  let setSettingsTry = await SetSettings(rapportFrequency, rapportVerbose);
  if (setSettingsTry[0]) {
    UpdateSettings();
  } else {
    alert("Something went wrong.");
    const errorReading = settings.errorcodes[setSettingsTry[1]];
    errorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + setSettingsTry[2] + " )") : "Couldn't access server.";
  }

  // Hide loading spinner
  loadingSpinner.style.display = "none";
});


// Handle new SMTP auth 'Form' (not a <form> as it is on multiple <th> but behaves like one thanks to multiple listeners)
formNew.addEventListener('submit', async (event) => {
  event.preventDefault();

  // Show loading spinner
  smtpLoadingSpinner.style.display = "block";

  const email = formNew.querySelector('input[name="newusername"]').value;
  const description = formNew.querySelector('input[name="newdescribe"]').value;

  // Clear previous & show messages
  smtpErrorMessage.textContent = "";
  if (email === "") {
    smtpErrorMessage.textContent += "Please enter an email. ";
  }
  if (smtpErrorMessage.textContent != "") {
    // Hide loading spinner
    smtpLoadingSpinner.style.display = "none";
    return;
  };

  // Handle the API response
  let addAuthTry = await AddSMTP(email, description);
  if (addAuthTry[0]) {
    UpdateSMTPList();
  } else {
    alert("Something went wrong.");
    const errorReading = settings.errorcodes[addAuthTry[1]];
    smtpErrorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + addAuthTry[2] + " )") : "Couldn't access server.";
  }

  // Hide loading spinner
  smtpLoadingSpinner.style.display = "none";
});

// Listen for "enter" keydown events on the input fields
formNew.querySelector('input[name="newusername"]').addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    event.preventDefault();
    formNew.dispatchEvent(new Event('submit'));
  }
});
formNew.querySelector('input[name="newdescribe"]').addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    event.preventDefault();
    formNew.dispatchEvent(new Event('submit'));
  }
});

// Listen for "click" events on the "Add" button
formNew.querySelector('button[type="submit"]').addEventListener('click', (event) => {
  event.preventDefault();
  formNew.dispatchEvent(new Event('submit'));
});
